from eventsd.events import models, forms
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import (
    FormView,
    DetailView,
    View,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy


class SignupView(LoginRequiredMixin, FormView):
    form_class = forms.EventSignupForm
    template_name = 'eventsd/events/event-signup.html'

    def get_success_url(self):
        return reverse_lazy('eventsd_events:attendee-detail', args=[self.attendee_id])

    def get_form_object(self):
        self.event = get_object_or_404(models.Event.objects.open_events, id=self.kwargs['pk'])
        return self.event.form

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        self.form_object = kwargs['form_object'] = self.get_form_object()
        return kwargs
    
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['event'] = self.event
        return context
    
    def form_valid(self, form):
        cleaned_data = form.cleaned_data
        first_name = cleaned_data.pop('first_name')
        last_name = cleaned_data.pop('last_name')
        dob = cleaned_data.pop('dob')
        attendee = models.Attendee(
            first_name=first_name,
            last_name=last_name,
            dob=dob,
            event=self.event,
            owner_id=self.request.user.id,
            status=models.SelectionStatus.ENROLLED,
        )
        attendee.save()
        self.attendee_id = attendee.id

        for qid in self.form_object.get_form_fields().keys():
            models.FormAnswer.objects.create(
                attendee=attendee,
                question_id=int(qid),
                answer=cleaned_data[qid],
            )

        return super().form_valid(form)


class AttendeeDetailView(LoginRequiredMixin, DetailView):
    template_name = 'eventsd/events/attendee-detail.html'

    def get_queryset(self):
        if self.request.user.has_perm("eventsd_events.can_view_attendee"):
            return models.Attendee.objects.all()
        return models.Attendee.objects.filter(owner_id=self.request.user.id)

class ConfirmAttendanceView(LoginRequiredMixin, View):
    http_method_names = ('post', )

    def post(self, request, *args, **kwargs):
        attendee = get_object_or_404(
            models.Attendee,
            owner_id=self.request.user.id,
            id=self.kwargs['pk'],
            status=models.SelectionStatus.ACCEPTED.value,
        )
        attendee.status = models.SelectionStatus.CONFIRMED.value
        attendee.save()
        return redirect(
            '#'.join((
                reverse('eventsd_events:attendee-detail', args=[attendee.pk]),
                'event-documents'
            ))
        )