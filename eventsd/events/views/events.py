from django.views.generic import (
    ListView,
    DetailView,
)

from eventsd.events import models

class UpcomingEventListView(ListView):
    template_name = 'eventsd/events/upcoming-events.html'

    def get_queryset(self):
        return models.Event.objects.open_events.prefetch_related()

class EventDetailView(DetailView):
    template_name = 'eventsd/events/event-detail.html'
    model = models.Event