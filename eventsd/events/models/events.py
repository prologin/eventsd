from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import ValidationError
from django.conf import settings
from django.core.files.storage import default_storage
from pathlib import Path
from django.utils import timezone
from .signup import SelectionStatus


class Center(models.Model):
    name = models.CharField(verbose_name=_("Nom"), max_length=120)
    address = models.TextField(verbose_name=_("Adresse"), max_length=1000)
    lng = models.DecimalField(
        verbose_name=_("Longitude"),
        max_digits=14,
        decimal_places=10,
        null=True,
        blank=True,
    )
    lat = models.DecimalField(
        verbose_name=_("Latitude"),
        max_digits=14,
        decimal_places=10,
        null=True,
        blank=True,
    )
    private_notes = models.TextField(
        verbose_name=("Notes privées"), max_length=2000, blank=True, null=True
    )

    class Meta:
        verbose_name = _("centre")
        verbose_name_plural = _("centres")

    def __str__(self):
        return self.name


class EventManager(models.Manager):
    @property
    def open_events(self):
        return self.filter(
            signup_start_date__lte=timezone.now(),
            signup_end_date__gt=timezone.now(),
        )

class Event(models.Model):
    name = models.CharField(
        max_length=256,
        verbose_name=_("Nom de l'évènement"),
    )

    center = models.ForeignKey(
        to="eventsd_events.Center",
        verbose_name=_("Centre"),
        on_delete=models.CASCADE,
    )

    signup_start_date = models.DateTimeField(
        verbose_name=_("Date de début d'inscription")
    )
    signup_end_date = models.DateTimeField(
        verbose_name=_("Date de fin d'inscription")
    )

    start_date = models.DateTimeField(verbose_name=_("Date de début"))
    end_date = models.DateTimeField(verbose_name=_("Date de fin"))

    form = models.ForeignKey(
        to="eventsd_events.Form",
        verbose_name=_("Formulaire"),
        on_delete=models.SET_DEFAULT,
        default=1,
    )

    notes = models.TextField(
        verbose_name=_("Notes"),
        help_text=_(
            "Ce texte, si défini est affiché "
            "lors de l'inscription à l'évènement "
            "(il peut éventuellement contenir des liens)"
        ),
        blank=True,
        null=True,
    )

    description = models.TextField(
        verbose_name=_("Description de l'évènement"),
        help_text=_(
            "Si ce champ n'est pas spécifié, "
            "la description du projet sera utilisée."
        ),
    )

    objects = EventManager()

    class Meta:
        verbose_name = _("évènement")
        verbose_name_plural = _("évènements")

    def clean(self):
        errors = {}

        if self.signup_start_date > self.signup_end_date:
            errors["signup_start_date"] = _(
                "La date début d'inscription doit être avant la date de "
                "fin d'inscription."
            )

        if self.signup_end_date > self.start_date:
            errors["signup_end_date"] = _(
                "La date de fin d'inscription doit être avant "
                "la date de début d'évènement."
            )

        if self.start_date > self.end_date:
            errors["end_date"] = _(
                "La date de fin d'évènement doit être après "
                "la date de début d'évènement."
            )

        if errors:
            raise ValidationError(errors)

        return self.form.questions.all()
    
    def signups_are_open(self):
        return self.signup_start_date < timezone.now()  and timezone.now() < self.signup_end_date

    def __str__(self):
        return self.name