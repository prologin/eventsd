from django.views.generic import TemplateView
from .mixins import StaffRequiredMixin

class AdminHomeView(StaffRequiredMixin, TemplateView):
    template_name = 'eventsd/admin/home.html'