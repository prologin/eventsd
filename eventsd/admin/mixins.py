from django.contrib.auth.mixins import AccessMixin
from django.utils.translation import gettext_lazy as _

class StaffRequiredMixin(AccessMixin):
    permission_denied_message = _(
        "Vous n'avez pas les autorisations nécessaires "
        "pour accéder à cette page."
    )

    def dispatch(self, request, *args, **kwargs):
        if all((self.request.user.is_authenticated, self.request.user.is_staff)):
            return super().dispatch(request, *args, **kwargs)
        return self.handle_no_permission()