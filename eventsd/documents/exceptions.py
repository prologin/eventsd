class CompilationException(Exception):
    def __init__(self, message, returncode, stdout, stderr):
        self.message = message
        self.returncode = returncode
        self.stdout = stdout.decode('utf-8', 'replace')
        self.stderr = stderr.decode('utf-8', 'replace')