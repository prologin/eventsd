from django.http.response import HttpResponse, HttpResponseServerError
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView
from eventsd.documents import models
from eventsd.documents.exceptions import CompilationException

class BasePDFDocumentView(TemplateView):
    """
    Base view that handles the TeX generation.
    """
    content_type = 'application/pdf'
    pdf_title = 'document'
    filename = 'document'
    extension = '.pdf'

    @staticmethod
    def escape_filename(filename):
        # quick safeguard, nothing inherently safe/secure, because it's shit: http://greenbytes.de/tech/tc2231/
        return filename.replace('"', '_')

    def apply_headers(self, response):
        filename = self.escape_filename(self.get_filename())
        disposition = 'attachment; ' if self.request.GET.get('dl') is not None else ''
        response['Content-Disposition'] = '{}filename="{}"'.format(disposition, filename)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.get_extra_context())
        return context

    def render_to_response(self, context, **response_kwargs):
        context['pdf_title'] = self.get_pdf_title()
        try:
            with models.generate_tex_pdf(self.get_template_names()[0], context) as output:
                response = HttpResponse(content=open(output, 'rb'), content_type=self.content_type)
                self.apply_headers(response)
                return response
        except CompilationException as error:
            if self.can_display_tex_errors():
                # Custom 500 error with stdout/stderr
                return self.response_class(self.request,
                                           template='eventsd/documents/tex-error-debug.html',
                                           context={'error': error},
                                           status=500)
            return HttpResponseServerError()

    def can_display_tex_errors(self) -> bool:
        """
        Whetere the use can see the TeX generation output.
        Warning: security hazard.
        """
        return self.request.user.has_perm('eventsd_documents.view_tex_compilation_errors')

    def get_pdf_title(self) -> str:
        """
        Override to customize the PDF title (meta-property of the file itself, read by PDF readers).
        """
        return self.pdf_title % self.i18nargs()

    def get_filename(self) -> str:
        """
        Override to customize the file name of the generated PDF file, as displayed in the browser's file save dialog.
        """
        return self.filename % {k: slugify(v) for k, v in self.i18nargs().items()} + self.extension

    def i18nargs(self) -> dict:
        """
        Override to customize the context catalogue for pdf_title and filename.
        """
        return {}

    def get_extra_context(self) -> dict:
        """
        Override to add extra context.
        """
        return {}